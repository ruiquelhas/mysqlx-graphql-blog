'use strict';

const Code = require('code');
const Config = require('config');
const Database = require('lib/db');
const GraphQL = require('lib/graphql/plugin');
const Hapi = require('hapi');
const Lab = require('lab');
const Qs = require('querystring');

const lab = exports.lab = Lab.script();

lab.experiment('mysqlx-graphql-blog', () => {

    let connection;
    let db;
    let server;

    lab.before((done) => {

        Database
            .connect(Config.database)
            .then((session) => {

                connection = session;
                db = session.getSchema(Config.database.schema);

                server = new Hapi.Server();
                server.app.db = db;
                server.connection();
                server.register(GraphQL);

                done();
            })
            .catch((err) => done(err));
    });

    lab.after((done) => {

        connection.close()
            .then(() => done())
            .catch((err) => done(err));
    });

    lab.experiment('authors', () => {

        lab.beforeEach((done) => {

            db.getCollection('authors')
                .remove('true')
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.beforeEach((done) => {

            db.getCollection('authors')
                .add({ _id: '1', firstname: 'John', lastname: 'Doe', age: 50, verified: false })
                .add({ _id: '2', firstname: 'John', lastname: 'Smith', age: 45, verified: false })
                .add({ _id: '3', firstname: 'Rui', lastname: 'Quelhas', age: 30, verified: true })
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.afterEach((done) => {

            db.getCollection('authors')
                .remove('true')
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.test('fetches a single author given its id', (done) => {

            const query = Qs.stringify({ query: '{ author(id:"1") { firstname, lastname } }' });
            const expected = JSON.stringify({ data: { author: { firstname: 'John', lastname: 'Doe' } } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });

        lab.test('returns null if an author does not exist', (done) => {

            const query = Qs.stringify({ query: '{ author(id:"4") { firstname } }' });
            const expected = JSON.stringify({ data: { author: null } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });

        lab.test('fetches a list of authors given a criteria', (done) => {

            const query = Qs.stringify({ query: '{ authors(firstname:"John", verified:false) { lastname } }' });
            const expected = JSON.stringify({ data: { authors: [{ lastname: 'Doe' }, { lastname: 'Smith' }] } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });

        lab.test('returns an empty list if no authors match a given criteria', (done) => {

            const query = Qs.stringify({ query: '{ authors(firstname:"foobar") { lastname } }' });
            const expected = JSON.stringify({ data: { authors: [] } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });
    });

    lab.experiment('posts', () => {

        lab.beforeEach((done) => {

            db.getCollection('posts')
                .remove('true')
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.beforeEach((done) => {

            db.getCollection('posts')
                .add({ _id: '1', title: 'foo bar', content: 'lorem ipsum' })
                .add({ _id: '2', title: 'baz qux', content: 'lorem ipsum dolor' })
                .add({ _id: '3', title: 'quux foo', content: 'lorem ipsum dolor sit amet' })
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.afterEach((done) => {

            db.getCollection('posts')
                .remove('true')
                .execute()
                .then(() => done())
                .catch((err) => done(err));
        });

        lab.test('fetches a list of posts that partially match the given title', (done) => {

            const query = Qs.stringify({ query: '{ posts(title:"foo") { content } }' });
            const expected = JSON.stringify({ data: { posts: [{ content: 'lorem ipsum' }, { content: 'lorem ipsum dolor sit amet' }] } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });

        lab.test('returns an empty list if no post title contains the given expression', (done) => {

            const query = Qs.stringify({ query: '{ posts(title:"biz") { content } }' });
            const expected = JSON.stringify({ data: { posts: [] } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });

        lab.test('returns a paginated list of results', (done) => {

            const query = Qs.stringify({ query: '{ posts(first:2, offset:2) { title } }' });
            const expected = JSON.stringify({ data: { posts: [{ title: 'quux foo' }] } });

            server.inject(`/graphql?${query}`, (response) => {

                Code.expect(response.statusCode).to.equal(200);
                Code.expect(response.result).to.equal(expected);

                done();
            });
        });
    });
});
