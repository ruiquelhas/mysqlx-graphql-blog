# mysqlx-graphql-blog

A sample blog backend using GraphQL, Node.js and the official MySQL X connector.

## Running the app

Install the required production dependencies:

```sh
$ npm install --production
```

Start a MySQL server with the X plugin enabled. Use the following environment variables to setup the server and database connection:

- `DB_HOST`     (default: `localhost`)
- `DB_PORT`     (default: `33060`)
- `DB_USER`     (default: `root`)
- `DB_PASSWORD` (default: `undefined`)
- `DB_SCHEMA`   (default: `mysqlx-graphql-blog`)
- `SERVER_PORT` (default: `3000`)

```sh
$ npm start
```

Open a browser window and navigate to `http://localhost:3000/graphiql` (using the default server configuration).

## Running the tests

Install all the required dependencies:

```sh
$ npm install
$ npm t
```
