'use strict';

const Config = require('config');
const Database = require('./db');
const Hapi = require('hapi');
const GraphQL = require('./graphql/plugin');

Database
    .connect(Config.database)
    .then((session) => {

        const server = new Hapi.Server();
        server.app.db = session.getSchema(Config.database.schema);
        server.connection(Config.server);
        server.register(GraphQL);

        return new Promise((resolve, reject) => {

            server.start((err) => {

                if (err) {
                    return reject(err);
                }

                resolve(server);
            });
        });
    })
    .then((server) => {

        console.log(`Server running at: ${server.info.uri}/graphiql`);
    })
    .catch((err) => {

        throw err;
    });
