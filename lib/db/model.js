'use strict';

const Utils = require('./utils');

module.exports = (collection) => ({
    findAll(criteria) {

        const results = [];

        return collection
            .find(Utils.buildCompExpr(criteria))
            .limit(criteria.first, criteria.offset)
            .execute((doc) => doc && results.push(doc))
            .then(() => results);
    },

    findById(id) {

        const results = [];

        return collection
            .find(Utils.buildCompExpr({ _id: id }))
            .execute((doc) => doc && results.push(doc))
            .then(() => results[0]);
    },

    findLike(criteria) {

        const results = [];

        return collection
            .find(Utils.buildCompExpr(criteria, { comparator: 'like', partial: true }))
            .limit(criteria.first, criteria.offset)
            .execute((doc) => doc && results.push(doc))
            .then(() => results);
    }
});
