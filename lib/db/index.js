'use strict';

const MySQL = require('@mysql/xdevapi');

exports.connect = function (options) {

    return MySQL.getSession(options)
        .then((session) => {

            const db = session.getSchema(options.schema);
            const setup = [
                db.createCollection('authors', { ReuseExistingObject: true }),
                db.createCollection('posts', { ReuseExistingObject: true })
            ];

            return Promise.all(setup).then(() => session);
        });
};
