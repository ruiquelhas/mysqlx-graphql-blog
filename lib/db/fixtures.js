'use strict';

const Faker = require('faker');
const Flatten = require('lodash.flatten');
const Times = require('lodash.times');

const internals = {};

internals.generateAuthors = function () {

    return Times(10, (index) => ({
        _id: `${index + 1}`,
        email: Faker.internet.email(),
        firstname: Faker.name.firstName(),
        lastname: Faker.name.lastName(),
        age: Faker.random.number({ min: 18, max: 65 }),
        verified: Faker.random.boolean()
    }));
};

exports.generateAuthors = internals.generateAuthors;

exports.generatePosts = function (authors) {

    authors = authors || internals.generateAuthors();

    return Flatten(authors.map((author) => {

        return Times(Faker.random.number({ min: 0, max: 3 }), () => ({
            author,
            title: Faker.lorem.sentence(),
            content: Faker.lorem.paragraphs(),
            tags: Times(Faker.random.number({ min: 1, max: 5 }), Faker.lorem.word)
        }));
    }));
};
