'use strict';

const internals = {};

internals.stringify = function (data, options) {

    options = Object.assign({ partial: false }, options);

    return (typeof data !== 'string') ? data : !options.partial ? `"${data}"` : `"%${data}%"`;
};

exports.buildCompExpr = function (criteria, options) {

    options = Object.assign({ aggregator: '&&', comparator: '==', partial: false }, options);

    const omit = ['first', 'offset'];
    const keys = Object.keys(criteria).filter((key) => omit.indexOf(key) === -1);

    return keys.reduce((result, key, index) => {

        const compExpr = result
            .concat('(')
            .concat(`$.${key}`)
            .concat(options.comparator)
            .concat(internals.stringify(criteria[key], { partial: options.partial }))
            .concat(')');

        if (index === keys.length - 1) {
            return compExpr;
        }

        return result.concat(compExpr).concat(options.aggregator);
    }, []).join(' ');
};
