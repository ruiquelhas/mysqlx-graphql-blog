'use strict';

const Model = require('../db/model');

module.exports = {
    author(args, request) {

        return Model(request.server.app.db.getCollection('authors')).findById(args.id);
    },

    authors(args, request) {

        return Model(request.server.app.db.getCollection('authors')).findAll(args);
    },

    posts(args, request) {

        return Model(request.server.app.db.getCollection('posts')).findLike(args);
    }
};
