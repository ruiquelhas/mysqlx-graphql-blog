'use strict';

const Graphi = require('graphi');
const Schema = require('./schema');
const Resolvers = require('./resolvers');

module.exports = {
    register: Graphi,
    options: {
        schema: Schema,
        resolvers: Resolvers
    }
};
