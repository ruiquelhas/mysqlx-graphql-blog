'use strict';

module.exports = `
    type Author {
        _id: String!
        email: String!
        firstname: String!
        lastname: String!
        age: Int
        verified: Boolean
        posts: [Post]
    }

    type Post {
        _id: String!
        author: Author!
        title: String!
        content: String!
        tags: [String]
    }

    type Query {
        author(id: String!): Author
        authors(firstname: String, lastname: String, age: Int, verified: Boolean, first: Int = 10, offset: Int = 0): [Author]!
        posts(title: String, first: Int = 10, offset: Int = 0): [Post]!
    }
`;
