#!/usr/bin/env node

'use strict';

const Config = require('config');
const Database = require('../lib/db');
const Fixtures = require('../lib/db/fixtures');

const authors = Fixtures.generateAuthors();
const posts = Fixtures.generatePosts(authors);

const script = Database
    .connect(Config.database)
    .then((session) => {

        return session
            .getSchema(Config.database.schema)
            .getCollection('posts')
            .remove('true')
            .execute()
            .then(() => session);
    })
    .then((session) => {

        return session
            .getSchema(Config.database.schema)
            .getCollection('authors')
            .remove('true')
            .execute()
            .then(() => session);
    })
    .then((session) => {

        return session
            .getSchema(Config.database.schema)
            .getCollection('authors')
            .add(authors)
            .execute()
            .then(() => session);
    })
    .then((session) => {

        return session
            .getSchema(Config.database.schema)
            .getCollection('posts')
            .add(posts)
            .execute()
            .then(() => session);
    })
    .then((session) => {

        return session.close();
    })
    .then(() => {

        return '>> Database populated';
    })
    .catch((err) => {

        throw err;
    });

Promise.all([script])
    .then((result) => {

        console.log(result[0]);
        process.exit(0);
    })
    .catch((err) => {

        console.error(err.message);
        process.exit(1);
    });
